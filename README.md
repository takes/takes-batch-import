### takes.io Batch import ###

Install `node`, make sure you got `npm` installed

    apt-get install node

Change directory to the project root

Install dependencies

    `npm install`

Type `./batchimport --help` to see the script's options

Example calls :

Add 3 videos to project 41WbkHyM :

```
./batchimport import \
    --api http://api.takes.io \
    --token f140cdee-d119-4bab-81a7-972d39380357 \
    --project 41WbkHyM \
    -v '[
        {"url" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "title" : "My title 1"},
        {"url" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "title" : "My title 2"},
        {"url" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "title" : "My title 3"}
    ]'
```

Add 3 versions to media Ek-4v641z :

```
./batchimport import \
    --api http://api.takes.io \
    --token f140cdee-d119-4bab-81a7-972d39380357 \
    --media Ek-4v641z \
    -v '[
        {"url" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "title" : "My title 1"},
        {"url" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "title" : "My title 2"},
        {"url" : "https://www.youtube.com/watch?v=dQw4w9WgXcQ", "title" : "My title 3"}
    ]'
```
